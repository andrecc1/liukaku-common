package com.cn.liukaku.common.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NumOperation
{
	public static void main (String[] args) throws java.lang.Exception
	{
		System.out.println(multiply1000("1234"));
	}

	public static String multiply1000(String numString){
		String multiply1000Value = numString;

		if(numString == null || ("".equals(numString.trim()))){
			return multiply1000Value !=null ? "" : null; 	
		}
		multiply1000Value = numString.trim();

		Pattern pattern = Pattern.compile("^-?[0-9]+\\.?[0-9]*");

		Matcher isNum = pattern.matcher(multiply1000Value);

		if(!isNum.matches()){
			// throw Exception	
		}

		BigDecimal bg1= new BigDecimal(multiply1000Value);
		BigDecimal bg2= new BigDecimal(1000);
		BigDecimal bg3= bg1.multiply(bg2);

		BigDecimal bg4= bg3.divide(new BigDecimal(1),0, RoundingMode.DOWN);
		multiply1000Value=bg4.toString();

		return multiply1000Value;
	}	
}