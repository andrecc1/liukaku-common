package com.cn.liukaku.common.hystrix;

import com.cn.liukaku.common.constants.HttpStatusConstants;
import com.cn.liukaku.common.dto.BaseResult;
import com.cn.liukaku.common.utils.MapperUtils;
import com.google.common.collect.Lists;

public class Fallback {

    /**
     * 通用熔断方法
     * @return
     */
    public static String badGateWay(){
        BaseResult baseResult = BaseResult.notOk(Lists.newArrayList(
                new BaseResult.Error(String.valueOf(HttpStatusConstants.BAD_GATEWAY.getStatus()),
                        HttpStatusConstants.BAD_GATEWAY.getContent())));
        try {
            return MapperUtils.obj2json(baseResult);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "服务器解析相应错误";
    }
}
